import os
import re
from setuptools import setup

# Set environment variable "VERSION_INCREMENT" to set next version jump
VERSION_INCREMENT_ENV = "VERSION_INCREMENT"

VERSION_INCREMENT_PATCH = 'patch'
VERSION_INCREMENT_MINOR = 'minor'
VERSION_INCREMENT_MAJOR = 'major'

DEFAULT_INCREMENT = VERSION_INCREMENT_MAJOR


def local_scheme(vers):
    return vers.format_choice(clean_format="+{node}", dirty_format="+{node}.dirty")


def version_scheme(vers):
    git_tag = str(vers.tag)
    parts = ['0', '0', '0']
    try:
        parts = list(re.search(r"v?(\d+\.\d+(\.\d+)?)", git_tag).group(1).split('.'))
        if not vers.exact:
            # Increment version
            increment = os.environ.get(VERSION_INCREMENT_ENV, DEFAULT_INCREMENT).lower()
            if len(parts) < 2:
                print("WARNING: Adding minor version to scheme that previously had none")
                parts.append('0')
            elif len(parts) < 3:
                print("WARNING: Adding patch version to scheme that previously had none")
                parts.append('0')
            try:
                index = {
                    VERSION_INCREMENT_PATCH: 2,
                    VERSION_INCREMENT_MINOR: 1,
                    VERSION_INCREMENT_MAJOR: 0,
                }[increment]
            except KeyError:
                raise SystemExit("env: %s must be one of %s, %s or %s" %
                                 (VERSION_INCREMENT_ENV, VERSION_INCREMENT_MAJOR,
                                  VERSION_INCREMENT_MINOR, VERSION_INCREMENT_PATCH))

            parts = parts[0:index] + [str(int(parts[index]) + 1)] + (['0'] * max(0, (2 - index)))

    except (IndexError, ValueError, AttributeError) as ex:
        if "'NoneType' object has no attribute 'group'" in str(ex):  # regex fail
            print("Parsing version number failed:", git_tag)
        else:
            print("Could not increment %s : %s" % (git_tag, ex))

    ver = "v" + ".".join(parts)
    return ver


setup(
    name="display_keyboard_trigger",
    author="Andrew Leech",
    author_email="andrew@alelec.net",
    description="Windows tool to turn OpenRGB lights on/off with monitor state",
    py_modules=['display_trigger.py'],
    use_scm_version = {
        "root": "../..",
        "relative_to": __file__,
        "local_scheme": local_scheme,
        "version_scheme": version_scheme
    },
    setup_requires=['setuptools_scm', 'pyoxidizer'],
    install_requires=[
        "openrgb-python>=0.2.8",
        "pypiwin32>=223",
        "comtypes>=1.1.10"
    ]
)
