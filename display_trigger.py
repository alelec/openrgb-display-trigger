## TODO
# Get rid of win32gui with https://stackoverflow.com/a/53619253 and https://gist.github.com/mouseroot/6128651
import sys
from ctypes import *
from ctypes import wintypes as w  # ctypes has many pre-defined Windows types

import time
from ctypes import POINTER, windll, Structure, cast, CFUNCTYPE, c_int, c_uint, c_void_p, c_bool
from comtypes import GUID
from ctypes.wintypes import HANDLE, DWORD

from openrgb import OpenRGBClient
from openrgb.utils import RGBColor, DeviceType


def errcheck(result,func,args):
    if result is None or result == 0:
        raise WinError(get_last_error())
    return result


# Missing from ctypes.wintypes...
LRESULT = c_int64
HCURSOR = c_void_p

WNDPROC = WINFUNCTYPE(LRESULT,w.HWND,w.UINT,w.WPARAM,w.LPARAM)

def MAKEINTRESOURCEW(x):
    return w.LPCWSTR(x)


PBT_POWERSETTINGCHANGE = 0x8013
GUID_CONSOLE_DISPLAY_STATE = '{6FE69556-704A-47A0-8F24-C28D936FDA47}'
GUID_ACDC_POWER_SOURCE = '{5D3E9A59-E9D5-4B00-A6BD-FF34FF516548}'
GUID_BATTERY_PERCENTAGE_REMAINING = '{A7AD8041-B45A-4CAE-87A3-EECBB468A9E1}'
GUID_MONITOR_POWER_ON = '{02731015-4510-4526-99E6-E5A17EBD1AEA}'
GUID_SYSTEM_AWAYMODE = '{98A7F580-01F7-48AA-9C0F-44352C29E5C0}'


class WNDCLASSW(Structure):
    _fields_ = [('style', w.UINT),
                ('lpfnWndProc', WNDPROC),
                ('cbClsExtra', c_int),
                ('cbWndExtra', c_int),
                ('hInstance', w.HINSTANCE),
                ('hIcon', w.HICON),
                ('hCursor', HCURSOR),
                ('hbrBackground', w.HBRUSH),
                ('lpszMenuName', w.LPCWSTR),
                ('lpszClassName', w.LPCWSTR)]

class PAINTSTRUCT(Structure):
    _fields_ = [('hdc', w.HDC),
                ('fErase', w.BOOL),
                ('rcPaint', w.RECT),
                ('fRestore', w.BOOL),
                ('fIncUpdate', w.BOOL),
                ('rgbReserved', w.BYTE * 32)]

kernel32 = WinDLL('kernel32',use_last_error=True)
kernel32.GetModuleHandleW.argtypes = w.LPCWSTR,
kernel32.GetModuleHandleW.restype = w.HMODULE
kernel32.GetModuleHandleW.errcheck = errcheck

user32 = WinDLL('user32',use_last_error=True)
user32.CreateWindowExW.argtypes = w.DWORD,w.LPCWSTR,w.LPCWSTR,w.DWORD,c_int,c_int,c_int,c_int,w.HWND,w.HMENU,w.HINSTANCE,w.LPVOID
user32.CreateWindowExW.restype = w.HWND
user32.CreateWindowExW.errcheck = errcheck
user32.LoadIconW.argtypes = w.HINSTANCE,w.LPCWSTR
user32.LoadIconW.restype = w.HICON
user32.LoadIconW.errcheck = errcheck
user32.LoadCursorW.argtypes = w.HINSTANCE,w.LPCWSTR
user32.LoadCursorW.restype = HCURSOR
user32.LoadCursorW.errcheck = errcheck
user32.RegisterClassW.argtypes = POINTER(WNDCLASSW),
user32.RegisterClassW.restype = w.ATOM
user32.RegisterClassW.errcheck = errcheck
user32.ShowWindow.argtypes = w.HWND,c_int
user32.ShowWindow.restype = w.BOOL
user32.UpdateWindow.argtypes = w.HWND,
user32.UpdateWindow.restype = w.BOOL
user32.UpdateWindow.errcheck = errcheck
user32.GetMessageW.argtypes = POINTER(w.MSG),w.HWND,w.UINT,w.UINT
user32.GetMessageW.restype = w.BOOL
user32.TranslateMessage.argtypes = POINTER(w.MSG),
user32.TranslateMessage.restype = w.BOOL
user32.DispatchMessageW.argtypes = POINTER(w.MSG),
user32.DispatchMessageW.restype = LRESULT
user32.BeginPaint.argtypes = w.HWND,POINTER(PAINTSTRUCT)
user32.BeginPaint.restype = w.HDC
user32.BeginPaint.errcheck = errcheck
user32.GetClientRect.argtypes = w.HWND,POINTER(w.RECT)
user32.GetClientRect.restype = w.BOOL
user32.GetClientRect.errcheck = errcheck
user32.DrawTextW.argtypes = w.HDC,w.LPCWSTR,c_int,POINTER(w.RECT),w.UINT
user32.DrawTextW.restype = c_int
user32.EndPaint.argtypes = w.HWND,POINTER(PAINTSTRUCT)
user32.EndPaint.restype = w.BOOL
user32.PostQuitMessage.argtypes = c_int,
user32.PostQuitMessage.restype = None
user32.DefWindowProcW.argtypes = w.HWND,w.UINT,w.WPARAM,w.LPARAM
user32.DefWindowProcW.restype = LRESULT

gdi32 = WinDLL('gdi32',use_last_error=True)
gdi32.GetStockObject.argtypes = c_int,
gdi32.GetStockObject.restype = w.HGDIOBJ


NULL = 0
CW_USEDEFAULT = -2147483648
IDI_APPLICATION = MAKEINTRESOURCEW(32512)
WS_OVERLAPPEDWINDOW = 13565952

CS_HREDRAW = 2
CS_VREDRAW = 1

IDC_ARROW = MAKEINTRESOURCEW(32512)
WHITE_BRUSH = 0

SW_SHOWNORMAL = 1

WM_PAINT = 15
WM_DESTROY = 2
WM_POWERBROADCAST = 536
DT_SINGLELINE = 32
DT_CENTER = 1
DT_VCENTER = 4

PBT_APMQUERYSUSPEND = 0
PBT_APMQUERYSTANDBY = 1
PBT_APMQUERYSUSPENDFAILED = 2
PBT_APMQUERYSTANDBYFAILED = 3
PBT_APMSUSPEND = 4
PBT_APMSTANDBY = 5
PBT_APMRESUMECRITICAL = 6
PBT_APMRESUMESUSPEND = 7
PBT_APMRESUMESTANDBY = 8
PBTF_APMRESUMEFROMFAILURE = 1
PBT_APMBATTERYLOW = 9
PBT_APMPOWERSTATUSCHANGE = 10
PBT_APMOEMEVENT = 11
PBT_APMRESUMEAUTOMATIC = 18

class POWERBROADCAST_SETTING(Structure):
    _fields_ = [("PowerSetting", GUID),
                ("DataLength", DWORD),
                ("Data", DWORD)]


def lights_on():
    global profile
    client = OpenRGBClient()
    client.load_profile(0)
    client.disconnect()


def lights_off():
    global profile
    
    client = OpenRGBClient()
    client.clear()
    client.disconnect()


def wndproc(hwnd, msg, wparam, lparam):
    if wparam == PBT_APMPOWERSTATUSCHANGE:
        print('Power status has changed')
    if wparam == PBT_APMRESUMEAUTOMATIC:
        print('System resume')
    if wparam == PBT_APMRESUMESUSPEND:
        print('System resume by user input')
    if wparam == PBT_APMSUSPEND:
        print('System suspend')
    if wparam == PBT_POWERSETTINGCHANGE:
        print('Power setting changed...')
        settings = cast(lparam, POINTER(POWERBROADCAST_SETTING)).contents
        power_setting = str(settings.PowerSetting)
        data_length = settings.DataLength
        data = settings.Data
        if power_setting == GUID_CONSOLE_DISPLAY_STATE:
            if data == 0: 
                print('Display off')
                lights_off()
            if data == 1: 
                print('Display on')
                lights_on()
            if data == 2: print('Display dimmed')
        elif power_setting == GUID_ACDC_POWER_SOURCE:
            if data == 0: print('AC power')
            if data == 1: print('Battery power')
            if data == 2: print('Short term power')
        elif power_setting == GUID_BATTERY_PERCENTAGE_REMAINING:
            print('battery remaining: %s' % data)
        elif power_setting == GUID_MONITOR_POWER_ON:
            if data == 0: 
                print('Monitor off')
                lights_off()
            if data == 1: 
                print('Monitor on')
                lights_on()
        elif power_setting == GUID_SYSTEM_AWAYMODE:
            if data == 0: print('Exiting away mode')
            if data == 1: print('Entering away mode')
        else:
            print('unknown GUID')
    return True


def WndProc(hwnd, message, wParam, lParam):
    if message == WM_POWERBROADCAST:
        wndproc(hwnd, message, wParam, lParam)
        return 0

    elif message == WM_DESTROY:
        user32.PostQuitMessage(0)
        return 0

    return user32.DefWindowProcW(hwnd,message,wParam,lParam)


if __name__ == "__main__":
    print("*** STARTING ***")

    wndclass = WNDCLASSW()
    wndclass.style          = CS_HREDRAW | CS_VREDRAW
    wndclass.lpfnWndProc    = WNDPROC(WndProc)
    wndclass.cbClsExtra = wndclass.cbWndExtra = 0
    wndclass.hInstance      = kernel32.GetModuleHandleW(None)
    wndclass.hIcon          = user32.LoadIconW(None, IDI_APPLICATION)
    wndclass.hCursor        = user32.LoadCursorW(None, IDC_ARROW)
    wndclass.hbrBackground  = gdi32.GetStockObject(WHITE_BRUSH)
    wndclass.lpszMenuName   = None
    wndclass.lpszClassName  = 'MainWin'



    # CMPFUNC = CFUNCTYPE(c_bool, c_int, c_uint, c_uint, c_void_p)
    # wndproc_pointer = CMPFUNC(wndproc)
    wndclass.lpfnWndProc = WNDPROC(WndProc)
    hwnd = None
    user32.RegisterClassW(byref(wndclass))
    hwnd = user32.CreateWindowExW(
        0,
        wndclass.lpszClassName, 
        "backgroundMsgWindow", 
        WS_OVERLAPPEDWINDOW, 
        CW_USEDEFAULT, 
        CW_USEDEFAULT, 
        CW_USEDEFAULT, 
        CW_USEDEFAULT, 
        None, 
        None, 
        wndclass.hInstance, 
        None
    )

    if hwnd is None:
        print("hwnd is none!")
    else:
        print("hwnd: %s" % hwnd)

    guids_info = {
                    'GUID_MONITOR_POWER_ON' : GUID_MONITOR_POWER_ON,
                    'GUID_SYSTEM_AWAYMODE' : GUID_SYSTEM_AWAYMODE,
                    'GUID_CONSOLE_DISPLAY_STATE' : GUID_CONSOLE_DISPLAY_STATE,
                    'GUID_ACDC_POWER_SOURCE' : GUID_ACDC_POWER_SOURCE,
                    'GUID_BATTERY_PERCENTAGE_REMAINING' : GUID_BATTERY_PERCENTAGE_REMAINING
                 }
    for name, guid_info in guids_info.items():
        result = windll.user32.RegisterPowerSettingNotification(HANDLE(hwnd), GUID(guid_info), DWORD(0))
        print('registering', name)
        print('result:', hex(result))
        print('lastError:', GetLastError())
        print()

    print('\nEntering loop')

    # Pump Messages
    msg = w.MSG()
    while user32.GetMessageW(byref(msg), None, 0, 0) != 0:
        user32.TranslateMessage(byref(msg))
        user32.DispatchMessageW(byref(msg))
        # win32gui.PumpWaitingMessages()
        time.sleep(0.25)
